/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package cat.xtec.ioc.calculadora;

import java.util.Scanner;

/**
 *
 * @author oriol
 */
public class Calculadora {
    public double num1;
    public double num2;
    
    public Calculadora(){
        Scanner scan=new Scanner(System.in);
        System.out.println("introdueix el num1");
        this.num1=scan.nextDouble();
        System.out.println("introdueix el num2");
        this.num2=scan.nextDouble();

        
    }
      public static double sumar(double a, double b) {
        return a + b;
    }
      public static double restar(double a,double b){
          return a-b;
      }
      public static double multiplicar(double a,double b){
          return a*b;
      }
      public static double dividir(double a,double b){
          if(b==0){
          throw new ArithmeticException("el divisor no pot ser 0");
          }
          return a/b;
      }

    public static void main(String[] args) {
        Calculadora calculadora=new Calculadora();
        System.out.println( Calculadora.sumar(calculadora.num1,calculadora.num2));
        System.out.println(Calculadora.restar(calculadora.num1, calculadora.num2));
        System.out.println(Calculadora.multiplicar(calculadora.num1, calculadora.num2));
        System.out.println(Calculadora.dividir(calculadora.num1, calculadora.num2));
        
    }
        
        
    
}
